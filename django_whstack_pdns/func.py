import json

import idna
import requests
from django.conf import settings


def get_from_powerdns_api(
    endpoint: str = "zones", server_id: str = "localhost", item: str = ""
):
    url_get = (
        settings.POWERDNS_AUTH_API_URL + "/api/v1/servers/" + server_id + "/" + endpoint
    )

    if not item == "" or not item is None:
        url_get += "/" + item

    r = requests.get(
        url_get,
        headers={
            "X-API-Key": settings.POWERDNS_AUTH_API_KEY,
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
    )

    return r.json()


def forward_to_powerdns_api(
    endpoint: str = "zones", server_id: str = "localhost", data: dict = dict
):
    r = requests.post(
        settings.POWERDNS_AUTH_API_URL
        + "/api/v1/servers/"
        + server_id
        + "/"
        + endpoint,
        headers={
            "X-API-Key": settings.POWERDNS_AUTH_API_KEY,
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        json=data,
    )

    return r.json()


def replace_on_powerdns_api(
    endpoint: str = "zones", server_id: str = "localhost", data: dict = dict
):
    r = requests.put(
        settings.POWERDNS_AUTH_API_URL
        + "/api/v1/servers/"
        + server_id
        + "/"
        + endpoint,
        headers={
            "X-API-Key": settings.POWERDNS_AUTH_API_KEY,
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        json=data,
    )

    if r.status_code == 204:
        return True

    return r.json()


def replace2_on_powerdns_api(
    endpoint: str = "zones", server_id: str = "localhost", data: dict = dict
):
    r = requests.patch(
        settings.POWERDNS_AUTH_API_URL
        + "/api/v1/servers/"
        + server_id
        + "/"
        + endpoint,
        headers={
            "X-API-Key": settings.POWERDNS_AUTH_API_KEY,
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        json=data,
    )

    if r.status_code == 204:
        return True

    return r.json()


def create_zone_from_template(source_zone: str, destination_zone: str):
    source_zone = idna.encode(source_zone).decode("utf-8")
    destination_zone = idna.encode(destination_zone).decode("utf-8")

    if not source_zone[-1] == ".":
        source_zone += "."

    if not destination_zone[-1] == ".":
        destination_zone += "."

    template_zone = get_from_powerdns_api("zones", "localhost", source_zone)
    template_rrset = template_zone["rrsets"]

    template_rrset_string = json.dumps(template_rrset)
    template_rrset_string = template_rrset_string.replace(
        source_zone[:-1], destination_zone[:-1]
    )
    template_rrset = json.loads(template_rrset_string)

    zone_data = {
        "kind": "native",
        "api_rectify": True,
        "dnssec": True,
        "name": destination_zone,
        "nameservers": list(),
        "rrsets": template_rrset,
    }

    # Create new zone
    new_zone = forward_to_powerdns_api("zones", "localhost", zone_data)

    return new_zone
