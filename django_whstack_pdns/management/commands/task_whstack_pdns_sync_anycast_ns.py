from django.core.management.base import BaseCommand

from django_whstack_pdns.django_whstack_pdns.tasks import whstack_pdns_sync_anycast_ns


class Command(BaseCommand):
    help = "Run task whstack_pdns_sync_anycast_ns"

    def handle(self, *args, **options):
        self.stdout.write(self.help)

        whstack_pdns_sync_anycast_ns()

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
