import csv

from django.core.management.base import BaseCommand

from django_powerdns_models.django_powerdns_models.models import Domains


class Command(BaseCommand):
    def handle(self, *args, **options):
        reader_domains = csv.DictReader(open("querydomainlist.csv"))

        domain_list = list()
        domain_list_sub = list()

        unknown_zones = list()

        for row in reader_domains:
            domain_list.append(row["DOMAIN"])

        domain_list.append("local")
        domain_list.append("arpa")
        domain_list.append("")

        for domain in domain_list:
            domain_list_sub.append("." + domain)

        dns_zones = Domains.objects.exclude(name__in=domain_list)

        for dns_zone in dns_zones:
            found = False

            for domain_list_sub_entry in domain_list_sub:
                if dns_zone.name.endswith(domain_list_sub_entry):
                    found = True

            if not found:
                unknown_zones.append(dns_zone)

        for unknown_zone in unknown_zones:
            print(unknown_zone.name)
