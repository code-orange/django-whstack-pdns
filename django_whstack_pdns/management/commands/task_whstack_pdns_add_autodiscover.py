from django.core.management.base import BaseCommand

from django_whstack_pdns.django_whstack_pdns.tasks import whstack_pdns_add_autodiscover


class Command(BaseCommand):
    help = "Run task whstack_pdns_add_autodiscover"

    def handle(self, *args, **options):
        self.stdout.write(self.help)

        whstack_pdns_add_autodiscover()

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
