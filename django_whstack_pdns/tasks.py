from django.db.models import Q

from django_powerdns_models.django_powerdns_models.models import (
    Domains,
    Records,
    Domainmetadata,
)
from django_whstack_models.django_whstack_models.tasks import *
from django_whstack_pdns.django_whstack_pdns.func import *


@shared_task(name="whstack_pdns_sync_dnssec")
def whstack_pdns_sync_dnssec():
    registry_domains = WhRegistryDomains.objects.all()

    for domain in registry_domains:
        dom_ksk = list()

        # Enable DNSSEC
        zone_meta = {
            "api_rectify": True,
            "dnssec": True,
        }

        replace_on_powerdns_api("zones/" + domain.domain, "localhost", zone_meta)

        crypto_key_list = get_from_powerdns_api(
            "zones", "localhost", domain.domain + "/cryptokeys"
        )

        if "error" in crypto_key_list:
            continue

        for crypto_key in crypto_key_list:
            if crypto_key["keytype"] == "ksk":
                dom_ksk.append(crypto_key["dnskey"])

        if len(dom_ksk) > 0:
            payload = {
                "s_login": settings.RRPPROXY_USER,
                "s_pw": settings.RRPPROXY_PASSWD,
                "command": "ModifyDomain",
                "domain": domain.domain,
            }

            for ksk in dom_ksk:
                payload["dnssec" + str(dom_ksk.index(ksk))] = ksk
        else:
            continue

        rrp_request = requests.get("https://api.rrpproxy.net/api/call", params=payload)

    return


@shared_task(name="whstack_pdns_set_soa_edit_api")
def whstack_pdns_set_soa_edit_api():
    all_zones = Domains.objects.filter(type="NATIVE").all()

    for zone in all_zones:
        try:
            soa_edit = zone.domainmetadata_set.get(kind="SOA-EDIT-API")
        except Domainmetadata.DoesNotExist:
            soa_edit = Domainmetadata(
                domain=zone, kind="SOA-EDIT-API", content="DEFAULT"
            )
            soa_edit.save(force_insert=True)

        try:
            soa_edit_dnsupdate = zone.domainmetadata_set.get(kind="SOA-EDIT-DNSUPDATE")
        except Domainmetadata.DoesNotExist:
            soa_edit_dnsupdate = Domainmetadata(
                domain=zone, kind="SOA-EDIT-DNSUPDATE", content="INCREASE"
            )
            soa_edit_dnsupdate.save(force_insert=True)

        try:
            api_rectify = zone.domainmetadata_set.get(kind="API-RECTIFY")
        except Domainmetadata.DoesNotExist:
            api_rectify = Domainmetadata(domain=zone, kind="API-RECTIFY", content="1")
            api_rectify.save(force_insert=True)

    return


@shared_task(name="whstack_pdns_add_caa")
def whstack_pdns_add_caa():
    all_zones = Domains.objects.filter(type="NATIVE").exclude(name__endswith=".arpa")

    for zone in all_zones:
        try:
            caa = zone.records_set.get(type="CAA", name=zone.name)
        except Records.MultipleObjectsReturned:
            continue
        except Records.DoesNotExist:
            domain_data = dict()
            domain_data["rrsets"] = list()

            record = dict()
            record["name"] = zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "CAA"
            record["ttl"] = 3600

            record["records"] = [
                {"content": '0 issue "letsencrypt.org"', "disabled": False}
            ]

            domain_data["rrsets"].append(record)

            replace2_on_powerdns_api("zones/" + zone.name, "localhost", domain_data)

    return


@shared_task(name="whstack_pdns_add_spf")
def whstack_pdns_add_spf():
    default_spf = '"v=spf1 mx include:srvfarm.net -all"'

    all_zones = Domains.objects.filter(type="NATIVE").exclude(name__endswith=".arpa")

    for zone in all_zones:
        domain_data = dict()
        domain_data["rrsets"] = list()

        if zone.name == ".":
            continue

        try:
            root_spf = zone.records_set.get(
                type="TXT", name=zone.name, content__istartswith='"v=spf1'
            )
        except Records.MultipleObjectsReturned:
            continue
        except Records.DoesNotExist:
            record = dict()
            record["name"] = zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "TXT"
            record["ttl"] = 3600

            record["records"] = list()

            record["records"].append({"content": default_spf, "disabled": False})

            for existing_txt_record in zone.records_set.filter(
                type="TXT", name=zone.name
            ):
                record["records"].append(
                    {"content": existing_txt_record.content, "disabled": False}
                )

            domain_data["rrsets"].append(record)

            root_spf_content = default_spf
        else:
            root_spf_content = root_spf.content

        sub_records = (
            zone.records_set.filter(
                type__in=(
                    "A",
                    "AAAA",
                ),
            )
            .exclude(
                name=zone.name,
            )
            .values("name")
            .distinct()
        )

        for sub_record in sub_records:
            sub_record_name = sub_record["name"]

            try:
                sub_spf = zone.records_set.get(
                    type="TXT", name=sub_record_name, content__istartswith='"v=spf1'
                )
            except Records.MultipleObjectsReturned:
                continue
            except Records.DoesNotExist:
                record = dict()
                record["name"] = sub_record_name + "."
                record["changetype"] = "REPLACE"
                record["type"] = "TXT"
                record["ttl"] = 3600

                record["records"] = list()

                record["records"].append(
                    {"content": root_spf_content, "disabled": False}
                )

                for existing_txt_record in zone.records_set.filter(
                    type="TXT", name=sub_record_name
                ):
                    record["records"].append(
                        {"content": existing_txt_record.content, "disabled": False}
                    )

                domain_data["rrsets"].append(record)

        if len(domain_data["rrsets"]) > 0:
            replace2_on_powerdns_api("zones/" + zone.name, "localhost", domain_data)

    return


@shared_task(name="whstack_pdns_add_dmarc")
def whstack_pdns_add_dmarc():
    all_zones = Domains.objects.filter(type="NATIVE").exclude(name__endswith=".arpa")

    for zone in all_zones:
        try:
            dmarc = zone.records_set.get(
                type="TXT", name="_dmarc." + zone.name, content__istartswith='"v=DMARC1'
            )
        except Records.MultipleObjectsReturned:
            continue
        except Records.DoesNotExist:
            domain_data = dict()
            domain_data["rrsets"] = list()

            record = dict()
            record["name"] = "_dmarc." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "TXT"
            record["ttl"] = 3600

            record["records"] = [
                {
                    "content": '"v=DMARC1; ' + "p=reject; " + "pct=100; " +
                    # 'rua=mailto:postmaster@' + zone.name + '; ' +
                    "rua=mailto:dmarc-reports@srvfarm.net; " +
                    # 'ruf=mailto:postmaster@' + zone.name + '; ' +
                    "ruf=mailto:dmarc-reports@srvfarm.net; "
                    + "fo=1:d:s; "
                    + "sp=reject; "
                    + "rf=afrf; "
                    + "ri=86400; "
                    + "adkim=r; "
                    + 'aspf=r"',
                    "disabled": False,
                }
            ]

            domain_data["rrsets"].append(record)

            replace2_on_powerdns_api("zones/" + zone.name, "localhost", domain_data)

    return


@shared_task(name="whstack_pdns_add_domainconnect")
def whstack_pdns_add_domainconnect():
    all_zones = Domains.objects.filter(type="NATIVE").exclude(name__endswith=".arpa")

    for zone in all_zones:
        try:
            domainconnect = zone.records_set.get(name="_domainconnect." + zone.name)
        except Records.MultipleObjectsReturned:
            continue
        except Records.DoesNotExist:
            domain_data = dict()
            domain_data["rrsets"] = list()

            record = dict()
            record["name"] = "_domainconnect." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "CNAME"
            record["ttl"] = 3600

            record["records"] = [
                {"content": "domainconnect.srvfarm.net.", "disabled": False}
            ]

            domain_data["rrsets"].append(record)

            replace2_on_powerdns_api("zones/" + zone.name, "localhost", domain_data)

    return


@shared_task(name="whstack_pdns_add_mx")
def whstack_pdns_add_mx():
    all_zones = Domains.objects.filter(type="NATIVE").exclude(name__endswith=".arpa")

    for zone in all_zones:
        mx_content = "mail.srvfarm.net."

        domain_data = dict()
        domain_data["rrsets"] = list()

        # Check for MX record
        try:
            mx = zone.records_set.get(name=zone.name, type="MX")
        except Records.MultipleObjectsReturned:
            mx_list = zone.records_set.filter(name=zone.name, type="MX")
            mx = mx_list[0]
            mx_content = mx.content
        except Records.DoesNotExist:
            record = dict()
            record["name"] = zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "MX"
            record["ttl"] = 3600

            record["records"] = [{"content": "10 " + mx_content, "disabled": False}]

            domain_data["rrsets"].append(record)
        else:
            mx_content = mx.content

        if not mx_content.endswith("."):
            mx_content += "."

        srv_records = {
            "_autodiscover._tcp": 443,
            "_imap._tcp": 0,
            "_imaps._tcp": 993,
            "_pop3._tcp": 0,
            "_pop3s._tcp": 995,
            "_submission._tcp": 587,
            "_caldav._tcp": 443,
            "_caldavs._tcp": 443,
            "_carddav._tcp": 443,
            "_carddavs._tcp": 443,
        }

        for srv_record, port in srv_records.items():
            # Check for SRV record
            try:
                srv_record_model = zone.records_set.get(
                    name=srv_record + "." + zone.name, type="SRV"
                )
            except Records.MultipleObjectsReturned:
                continue
            except Records.DoesNotExist:
                record = dict()
                record["name"] = srv_record + "." + zone.name + "."
                record["changetype"] = "REPLACE"
                record["type"] = "SRV"
                record["ttl"] = 3600

                record["records"] = [
                    {
                        "content": "0 0 " + str(port) + " " + mx_content,
                        "disabled": False,
                    }
                ]

                domain_data["rrsets"].append(record)

        if len(domain_data["rrsets"]) > 0:
            replace2_on_powerdns_api("zones/" + zone.name, "localhost", domain_data)

    return


@shared_task(name="whstack_pdns_add_kerberos")
def whstack_pdns_add_kerberos():
    kerberos_content = "INTRA.SRVFARM.NET"

    auth_servers = (
        "auth-host01.auth.srvfarm.net",
        "auth-host02.auth.srvfarm.net",
        "auth-host03.auth.srvfarm.net",
    )

    all_zones = Domains.objects.filter(type="NATIVE").exclude(name__endswith=".arpa")

    for zone in all_zones:
        domain_data = dict()
        domain_data["rrsets"] = list()

        # Check for kerberos TXT record
        try:
            kerberos_txt = zone.records_set.get(
                name="_kerberos." + zone.name, type="TXT"
            )
        except Records.MultipleObjectsReturned:
            pass
        except Records.DoesNotExist:
            record = dict()
            record["name"] = "_kerberos." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "TXT"
            record["ttl"] = 3600

            record["records"] = [
                {"content": '"' + kerberos_content + '"', "disabled": False}
            ]

            domain_data["rrsets"].append(record)
        else:
            pass

        # Check for kerberos master SRV records
        try:
            kerberos_master_srv = zone.records_set.get(
                Q(name="_kerberos-master._tcp." + zone.name)
                | Q(name="_kerberos-master._udp." + zone.name),
                type="SRV",
            )
        except Records.MultipleObjectsReturned:
            pass
        except Records.DoesNotExist:
            record = dict()
            record["name"] = "_kerberos-master._tcp." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "SRV"
            record["ttl"] = 3600

            record["records"] = list()

            for auth_server in auth_servers:
                if not auth_server.endswith("."):
                    auth_server = auth_server + "."

                new_record = {"content": "0 100 88 " + auth_server, "disabled": False}

                record["records"].append(new_record)

            domain_data["rrsets"].append(record)

            record = dict()
            record["name"] = "_kerberos-master._udp." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "SRV"
            record["ttl"] = 3600

            record["records"] = list()

            for auth_server in auth_servers:
                if not auth_server.endswith("."):
                    auth_server = auth_server + "."

                new_record = {"content": "0 100 88 " + auth_server, "disabled": False}

                record["records"].append(new_record)

            domain_data["rrsets"].append(record)
        else:
            pass

        # Check for kerberos SRV records
        try:
            kerberos_srv = zone.records_set.get(
                Q(name="_kerberos._tcp." + zone.name)
                | Q(name="_kerberos._udp." + zone.name),
                type="SRV",
            )
        except Records.MultipleObjectsReturned:
            pass
        except Records.DoesNotExist:
            record = dict()
            record["name"] = "_kerberos._tcp." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "SRV"
            record["ttl"] = 3600

            record["records"] = list()

            for auth_server in auth_servers:
                if not auth_server.endswith("."):
                    auth_server = auth_server + "."

                new_record = {"content": "0 100 88 " + auth_server, "disabled": False}

                record["records"].append(new_record)

            domain_data["rrsets"].append(record)

            record = dict()
            record["name"] = "_kerberos._udp." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "SRV"
            record["ttl"] = 3600

            record["records"] = list()

            for auth_server in auth_servers:
                if not auth_server.endswith("."):
                    auth_server = auth_server + "."

                new_record = {"content": "0 100 88 " + auth_server, "disabled": False}

                record["records"].append(new_record)

            domain_data["rrsets"].append(record)
        else:
            pass

        # Check for kerberos KDCProxy URI records
        try:
            kerberos_uri = zone.records_set.get(
                name="_kerberos." + zone.name, type="URI"
            )
        except Records.MultipleObjectsReturned:
            pass
        except Records.DoesNotExist:
            record = dict()
            record["name"] = "_kerberos." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "URI"
            record["ttl"] = 3600

            record["records"] = list()

            for auth_server in auth_servers:
                if auth_server.endswith("."):
                    auth_server = auth_server[:-1]

                new_record = {
                    "content": '10 1 "krb5srv:M:kkdcp:https://'
                    + auth_server
                    + '/KdcProxy"',
                    "disabled": False,
                }

                record["records"].append(new_record)

            domain_data["rrsets"].append(record)
        else:
            pass

        # Check for krb5kdc KDCProxy URI records
        try:
            krb5kdc_uri = zone.records_set.get(name="_krb5kdc." + zone.name, type="URI")
        except Records.MultipleObjectsReturned:
            pass
        except Records.DoesNotExist:
            record = dict()
            record["name"] = "_krb5kdc." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "URI"
            record["ttl"] = 3600

            record["records"] = list()

            for auth_server in auth_servers:
                if auth_server.endswith("."):
                    auth_server = auth_server[:-1]

                new_record = {
                    "content": '10 1 "krb5srv:M:kkdcp:https://'
                    + auth_server
                    + '/KdcProxy"',
                    "disabled": False,
                }

                record["records"].append(new_record)

            domain_data["rrsets"].append(record)
        else:
            pass

        # Check for kerberos-iv SRV records
        try:
            kerberos_iv_srv = zone.records_set.get(
                Q(name="_kerberos-iv._tcp." + zone.name)
                | Q(name="_kerberos-iv._udp." + zone.name),
                type="SRV",
            )
        except Records.MultipleObjectsReturned:
            pass
        except Records.DoesNotExist:
            record = dict()
            record["name"] = "_kerberos-iv._tcp." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "SRV"
            record["ttl"] = 3600

            record["records"] = list()

            for auth_server in auth_servers:
                if not auth_server.endswith("."):
                    auth_server = auth_server + "."

                new_record = {"content": "0 100 88 " + auth_server, "disabled": False}

                record["records"].append(new_record)

            domain_data["rrsets"].append(record)

            record = dict()
            record["name"] = "_kerberos-iv._udp." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "SRV"
            record["ttl"] = 3600

            record["records"] = list()

            for auth_server in auth_servers:
                if not auth_server.endswith("."):
                    auth_server = auth_server + "."

                new_record = {"content": "0 100 88 " + auth_server, "disabled": False}

                record["records"].append(new_record)

            domain_data["rrsets"].append(record)
        else:
            pass

        # Check for kerberos-adm SRV records
        try:
            kerberos_adm_srv = zone.records_set.get(
                Q(name="_kerberos-adm._tcp." + zone.name)
                | Q(name="_kerberos-adm._udp." + zone.name),
                type="SRV",
            )
        except Records.MultipleObjectsReturned:
            pass
        except Records.DoesNotExist:
            record = dict()
            record["name"] = "_kerberos-adm._tcp." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "SRV"
            record["ttl"] = 3600

            record["records"] = list()

            for auth_server in auth_servers:
                if not auth_server.endswith("."):
                    auth_server = auth_server + "."

                new_record = {"content": "0 100 749 " + auth_server, "disabled": False}

                record["records"].append(new_record)

            domain_data["rrsets"].append(record)

            record = dict()
            record["name"] = "_kerberos-adm._udp." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "SRV"
            record["ttl"] = 3600

            record["records"] = list()

            for auth_server in auth_servers:
                if not auth_server.endswith("."):
                    auth_server = auth_server + "."

                new_record = {"content": "0 100 749 " + auth_server, "disabled": False}

                record["records"].append(new_record)

            domain_data["rrsets"].append(record)
        else:
            pass

        # Check for kerberos-adm KDCProxy URI records
        try:
            kerberos_adm_uri = zone.records_set.get(
                name="_kerberos-adm." + zone.name, type="URI"
            )
        except Records.MultipleObjectsReturned:
            pass
        except Records.DoesNotExist:
            record = dict()
            record["name"] = "_kerberos-adm." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "URI"
            record["ttl"] = 3600

            record["records"] = list()

            for auth_server in auth_servers:
                if auth_server.endswith("."):
                    auth_server = auth_server[:-1]

                new_record = {
                    "content": '10 1 "krb5srv:M:kkdcp:https://'
                    + auth_server
                    + '/KdcProxy"',
                    "disabled": False,
                }

                record["records"].append(new_record)

            domain_data["rrsets"].append(record)
        else:
            pass

        # Check for kpasswd SRV records
        try:
            kpasswd_srv = zone.records_set.get(
                Q(name="_kpasswd._tcp." + zone.name)
                | Q(name="_kpasswd._udp." + zone.name),
                type="SRV",
            )
        except Records.MultipleObjectsReturned:
            pass
        except Records.DoesNotExist:
            record = dict()
            record["name"] = "_kpasswd._tcp." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "SRV"
            record["ttl"] = 3600

            record["records"] = list()

            for auth_server in auth_servers:
                if not auth_server.endswith("."):
                    auth_server = auth_server + "."

                new_record = {"content": "0 100 464 " + auth_server, "disabled": False}

                record["records"].append(new_record)

            domain_data["rrsets"].append(record)

            record = dict()
            record["name"] = "_kpasswd._udp." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "SRV"
            record["ttl"] = 3600

            record["records"] = list()

            for auth_server in auth_servers:
                if not auth_server.endswith("."):
                    auth_server = auth_server + "."

                new_record = {"content": "0 100 464 " + auth_server, "disabled": False}

                record["records"].append(new_record)

            domain_data["rrsets"].append(record)
        else:
            pass

        # Check for kpasswd KDCProxy URI records
        try:
            kpasswd_uri = zone.records_set.get(name="_kpasswd." + zone.name, type="URI")
        except Records.MultipleObjectsReturned:
            pass
        except Records.DoesNotExist:
            record = dict()
            record["name"] = "_kpasswd." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "URI"
            record["ttl"] = 3600

            record["records"] = list()

            for auth_server in auth_servers:
                if auth_server.endswith("."):
                    auth_server = auth_server[:-1]

                new_record = {
                    "content": '10 1 "krb5srv:M:kkdcp:https://'
                    + auth_server
                    + '/KdcProxy"',
                    "disabled": False,
                }

                record["records"].append(new_record)

            domain_data["rrsets"].append(record)
        else:
            pass

        if len(domain_data["rrsets"]) > 0:
            replace2_on_powerdns_api("zones/" + zone.name, "localhost", domain_data)

    return


@shared_task(name="whstack_pdns_add_ldap")
def whstack_pdns_add_ldap():
    auth_servers = (
        "ldap-lb1.ldap.srvfarm.net",
        "ldap-lb2.ldap.srvfarm.net",
    )

    all_zones = Domains.objects.filter(type="NATIVE").exclude(name__endswith=".arpa")

    for zone in all_zones:
        domain_data = dict()
        domain_data["rrsets"] = list()

        # Check for ldap SRV record
        try:
            ldap_srv = zone.records_set.get(name="_ldap._tcp." + zone.name, type="SRV")
        except Records.MultipleObjectsReturned:
            pass
        except Records.DoesNotExist:
            record = dict()
            record["name"] = "_ldap._tcp." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "SRV"
            record["ttl"] = 3600

            record["records"] = list()

            for auth_server in auth_servers:
                if not auth_server.endswith("."):
                    auth_server = auth_server + "."

                new_record = {"content": "0 100 389 " + auth_server, "disabled": False}

                record["records"].append(new_record)

            domain_data["rrsets"].append(record)
        else:
            pass

        # Check for ldaps SRV record
        try:
            ldaps_srv = zone.records_set.get(
                name="_ldaps._tcp." + zone.name, type="SRV"
            )
        except Records.MultipleObjectsReturned:
            pass
        except Records.DoesNotExist:
            record = dict()
            record["name"] = "_ldaps._tcp." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "SRV"
            record["ttl"] = 3600

            record["records"] = list()

            for auth_server in auth_servers:
                if not auth_server.endswith("."):
                    auth_server = auth_server + "."

                new_record = {"content": "0 100 636 " + auth_server, "disabled": False}

                record["records"].append(new_record)

            domain_data["rrsets"].append(record)
        else:
            pass

        if len(domain_data["rrsets"]) > 0:
            replace2_on_powerdns_api("zones/" + zone.name, "localhost", domain_data)

    return


@shared_task(name="whstack_pdns_add_ntp")
def whstack_pdns_add_ntp():
    ntp_servers = (
        "time1.srvfarm.net",
        "time2.srvfarm.net",
        "time3.srvfarm.net",
    )

    all_zones = Domains.objects.filter(type="NATIVE").exclude(name__endswith=".arpa")

    for zone in all_zones:
        domain_data = dict()
        domain_data["rrsets"] = list()

        # Check for ntp SRV record
        try:
            ntp_srv = zone.records_set.get(name="_ntp._udp." + zone.name, type="SRV")
        except Records.MultipleObjectsReturned:
            pass
        except Records.DoesNotExist:
            record = dict()
            record["name"] = "_ntp._udp." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "SRV"
            record["ttl"] = 3600

            record["records"] = list()

            for ntp_server in ntp_servers:
                if not ntp_server.endswith("."):
                    ntp_server = ntp_server + "."

                new_record = {"content": "0 100 123 " + ntp_server, "disabled": False}

                record["records"].append(new_record)

            domain_data["rrsets"].append(record)
        else:
            pass

        if len(domain_data["rrsets"]) > 0:
            replace2_on_powerdns_api("zones/" + zone.name, "localhost", domain_data)

    return


@shared_task(name="whstack_pdns_add_ctiserver")
def whstack_pdns_add_ctiserver():
    cti_servers = (
        "cti1.cti.srvfarm.net",
        "cti2.cti.srvfarm.net",
        "cti3.cti.srvfarm.net",
    )

    all_zones = Domains.objects.filter(type="NATIVE").exclude(name__endswith=".arpa")

    for zone in all_zones:
        domain_data = dict()
        domain_data["rrsets"] = list()

        # Check for ctiserver SRV record
        try:
            ctiserver_srv = zone.records_set.get(
                name="_ctiserver._tcp." + zone.name, type="SRV"
            )
        except Records.MultipleObjectsReturned:
            pass
        except Records.DoesNotExist:
            record = dict()
            record["name"] = "_ctiserver._tcp." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "SRV"
            record["ttl"] = 3600

            record["records"] = list()

            for cti_server in cti_servers:
                if not cti_server.endswith("."):
                    cti_server = cti_server + "."

                new_record = {"content": "1 0 7222 " + cti_server, "disabled": False}

                record["records"].append(new_record)

            domain_data["rrsets"].append(record)
        else:
            pass

        if len(domain_data["rrsets"]) > 0:
            replace2_on_powerdns_api("zones/" + zone.name, "localhost", domain_data)

    return


@shared_task(name="whstack_pdns_add_xmppserver")
def whstack_pdns_add_xmppserver():
    xmpp_servers = (
        "jabber-host01.talk.srvfarm.net",
        "jabber-host02.talk.srvfarm.net",
        "jabber-host03.talk.srvfarm.net",
    )

    all_zones = Domains.objects.filter(type="NATIVE").exclude(name__endswith=".arpa")

    for zone in all_zones:
        domain_data = dict()
        domain_data["rrsets"] = list()

        # Check for jabber SRV record
        try:
            jabber_srv = zone.records_set.get(
                name="_jabber._tcp." + zone.name, type="SRV"
            )
        except Records.MultipleObjectsReturned:
            pass
        except Records.DoesNotExist:
            record = dict()
            record["name"] = "_jabber._tcp." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "SRV"
            record["ttl"] = 3600

            record["records"] = list()

            for jabber_server in xmpp_servers:
                if not jabber_server.endswith("."):
                    jabber_server = jabber_server + "."

                new_record = {"content": "5 0 5269 " + jabber_server, "disabled": False}

                record["records"].append(new_record)

            domain_data["rrsets"].append(record)
        else:
            pass

        # Check for xmpp-server SRV record
        try:
            xmppserver_srv = zone.records_set.get(
                name="_xmpp-server._tcp." + zone.name, type="SRV"
            )
        except Records.MultipleObjectsReturned:
            pass
        except Records.DoesNotExist:
            record = dict()
            record["name"] = "_xmpp-server._tcp." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "SRV"
            record["ttl"] = 3600

            record["records"] = list()

            for xmpp_server in xmpp_servers:
                if not xmpp_server.endswith("."):
                    xmpp_server = xmpp_server + "."

                new_record = {"content": "5 0 5269 " + xmpp_server, "disabled": False}

                record["records"].append(new_record)

            domain_data["rrsets"].append(record)
        else:
            pass

        # Check for xmpps-server SRV record
        # XEP-0368: SRV records for XMPP over TLS
        try:
            xmppsserver_srv = zone.records_set.get(
                name="_xmpps-server._tcp." + zone.name, type="SRV"
            )
        except Records.MultipleObjectsReturned:
            pass
        except Records.DoesNotExist:
            record = dict()
            record["name"] = "_xmpps-server._tcp." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "SRV"
            record["ttl"] = 3600

            record["records"] = list()

            for xmpp_server in xmpp_servers:
                if not xmpp_server.endswith("."):
                    xmpp_server = xmpp_server + "."

                new_record = {"content": "5 0 5270 " + xmpp_server, "disabled": False}

                record["records"].append(new_record)

            domain_data["rrsets"].append(record)
        else:
            pass

        if len(domain_data["rrsets"]) > 0:
            replace2_on_powerdns_api("zones/" + zone.name, "localhost", domain_data)

    return


@shared_task(name="whstack_pdns_add_xmppclient")
def whstack_pdns_add_xmppclient():
    xmpp_client_servers = (
        "jabber-host01.talk.srvfarm.net",
        "jabber-host02.talk.srvfarm.net",
        "jabber-host03.talk.srvfarm.net",
    )

    all_zones = Domains.objects.filter(type="NATIVE").exclude(name__endswith=".arpa")

    for zone in all_zones:
        domain_data = dict()
        domain_data["rrsets"] = list()

        # Check for xmpp-client SRV record
        try:
            xmppclient_srv = zone.records_set.get(
                name="_xmpp-client._tcp." + zone.name, type="SRV"
            )
        except Records.MultipleObjectsReturned:
            pass
        except Records.DoesNotExist:
            record = dict()
            record["name"] = "_xmpp-client._tcp." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "SRV"
            record["ttl"] = 3600

            record["records"] = list()

            for xmpp_client_server in xmpp_client_servers:
                if not xmpp_client_server.endswith("."):
                    xmpp_client_server = xmpp_client_server + "."

                new_record = {
                    "content": "5 0 5222 " + xmpp_client_server,
                    "disabled": False,
                }

                record["records"].append(new_record)

            domain_data["rrsets"].append(record)
        else:
            pass

        # Check for xmpps-client SRV record
        # XEP-0368: SRV records for XMPP over TLS
        try:
            xmppsclient_srv = zone.records_set.get(
                name="_xmpps-client._tcp." + zone.name, type="SRV"
            )
        except Records.MultipleObjectsReturned:
            pass
        except Records.DoesNotExist:
            record = dict()
            record["name"] = "_xmpps-client._tcp." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "SRV"
            record["ttl"] = 3600

            record["records"] = list()

            for xmpp_client_server in xmpp_client_servers:
                if not xmpp_client_server.endswith("."):
                    xmpp_client_server = xmpp_client_server + "."

                new_record = {
                    "content": "5 0 5223 " + xmpp_client_server,
                    "disabled": False,
                }

                record["records"].append(new_record)

            domain_data["rrsets"].append(record)
        else:
            pass

        if len(domain_data["rrsets"]) > 0:
            replace2_on_powerdns_api("zones/" + zone.name, "localhost", domain_data)

    return


@shared_task(name="whstack_pdns_add_xmppconnect")
def whstack_pdns_add_xmppconnect():
    all_zones = Domains.objects.filter(type="NATIVE").exclude(name__endswith=".arpa")

    xmpp_domain = "talk.srvfarm.net"

    for zone in all_zones:
        try:
            xmppconnect_txt = zone.records_set.get(
                type="TXT", name="_xmppconnect." + zone.name
            )
        except Records.MultipleObjectsReturned:
            continue
        except Records.DoesNotExist:
            domain_data = dict()
            domain_data["rrsets"] = list()

            record = dict()
            record["name"] = "_xmppconnect." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "TXT"
            record["ttl"] = 3600

            record["records"] = [
                {
                    "content": '"_xmpp-client-xbosh=https://' + xmpp_domain + '/bosh"',
                    "disabled": False,
                },
                {
                    "content": '"_xmpp-client-websocket=wss://' + xmpp_domain + '/ws"',
                    "disabled": False,
                },
            ]

            domain_data["rrsets"].append(record)

            replace2_on_powerdns_api("zones/" + zone.name, "localhost", domain_data)

    return


@shared_task(name="whstack_pdns_add_stun")
def whstack_pdns_add_stun():
    stun_servers = (
        "turn01.turn.srvfarm.net",
        "turn02.turn.srvfarm.net",
        "turn03.turn.srvfarm.net",
    )

    all_zones = Domains.objects.filter(type="NATIVE").exclude(name__endswith=".arpa")

    for zone in all_zones:
        domain_data = dict()
        domain_data["rrsets"] = list()

        # Check for stun tcp SRV record
        try:
            stun_srv = zone.records_set.get(name="_stun._tcp." + zone.name, type="SRV")
        except Records.MultipleObjectsReturned:
            pass
        except Records.DoesNotExist:
            record = dict()
            record["name"] = "_stun._tcp." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "SRV"
            record["ttl"] = 3600

            record["records"] = list()

            for stun_server in stun_servers:
                if not stun_server.endswith("."):
                    stun_server = stun_server + "."

                new_record = {"content": "5 0 3478 " + stun_server, "disabled": False}

                record["records"].append(new_record)

            domain_data["rrsets"].append(record)
        else:
            pass

        # Check for stun udp SRV record
        try:
            stun_srv = zone.records_set.get(name="_stun._udp." + zone.name, type="SRV")
        except Records.MultipleObjectsReturned:
            pass
        except Records.DoesNotExist:
            record = dict()
            record["name"] = "_stun._udp." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "SRV"
            record["ttl"] = 3600

            record["records"] = list()

            for stun_server in stun_servers:
                if not stun_server.endswith("."):
                    stun_server = stun_server + "."

                new_record = {"content": "5 0 3478 " + stun_server, "disabled": False}

                record["records"].append(new_record)

            domain_data["rrsets"].append(record)
        else:
            pass

        # Check for stuns tcp SRV record
        try:
            stuns_srv = zone.records_set.get(
                name="_stuns._tcp." + zone.name, type="SRV"
            )
        except Records.MultipleObjectsReturned:
            pass
        except Records.DoesNotExist:
            record = dict()
            record["name"] = "_stuns._tcp." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "SRV"
            record["ttl"] = 3600

            record["records"] = list()

            for stun_server in stun_servers:
                if not stun_server.endswith("."):
                    stun_server = stun_server + "."

                new_record = {"content": "5 0 5349 " + stun_server, "disabled": False}

                record["records"].append(new_record)

                new_record = {"content": "10 0 443 " + stun_server, "disabled": False}

                record["records"].append(new_record)

            domain_data["rrsets"].append(record)
        else:
            pass

        if len(domain_data["rrsets"]) > 0:
            replace2_on_powerdns_api("zones/" + zone.name, "localhost", domain_data)

    return


@shared_task(name="whstack_pdns_add_turn")
def whstack_pdns_add_turn():
    turn_servers = (
        "turn01.turn.srvfarm.net",
        "turn02.turn.srvfarm.net",
        "turn03.turn.srvfarm.net",
    )

    all_zones = Domains.objects.filter(type="NATIVE").exclude(name__endswith=".arpa")

    for zone in all_zones:
        domain_data = dict()
        domain_data["rrsets"] = list()

        # Check for turn tcp SRV record
        try:
            turn_srv = zone.records_set.get(name="_turn._tcp." + zone.name, type="SRV")
        except Records.MultipleObjectsReturned:
            pass
        except Records.DoesNotExist:
            record = dict()
            record["name"] = "_turn._tcp." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "SRV"
            record["ttl"] = 3600

            record["records"] = list()

            for turn_server in turn_servers:
                if not turn_server.endswith("."):
                    turn_server = turn_server + "."

                new_record = {"content": "5 0 3478 " + turn_server, "disabled": False}

                record["records"].append(new_record)

            domain_data["rrsets"].append(record)
        else:
            pass

        # Check for turn udp SRV record
        try:
            turn_srv = zone.records_set.get(name="_turn._udp." + zone.name, type="SRV")
        except Records.MultipleObjectsReturned:
            pass
        except Records.DoesNotExist:
            record = dict()
            record["name"] = "_turn._udp." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "SRV"
            record["ttl"] = 3600

            record["records"] = list()

            for turn_server in turn_servers:
                if not turn_server.endswith("."):
                    turn_server = turn_server + "."

                new_record = {"content": "5 0 3478 " + turn_server, "disabled": False}

                record["records"].append(new_record)

            domain_data["rrsets"].append(record)
        else:
            pass

        # Check for turns tcp SRV record
        try:
            turns_srv = zone.records_set.get(
                name="_turns._tcp." + zone.name, type="SRV"
            )
        except Records.MultipleObjectsReturned:
            pass
        except Records.DoesNotExist:
            record = dict()
            record["name"] = "_turns._tcp." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "SRV"
            record["ttl"] = 3600

            record["records"] = list()

            for turn_server in turn_servers:
                if not turn_server.endswith("."):
                    turn_server = turn_server + "."

                new_record = {"content": "5 0 5349 " + turn_server, "disabled": False}

                record["records"].append(new_record)

                new_record = {"content": "10 0 443 " + turn_server, "disabled": False}

                record["records"].append(new_record)

            domain_data["rrsets"].append(record)
        else:
            pass

        if len(domain_data["rrsets"]) > 0:
            replace2_on_powerdns_api("zones/" + zone.name, "localhost", domain_data)

    return


@shared_task(name="whstack_pdns_add_matrix")
def whstack_pdns_add_matrix():
    matrix_servers = (
        "matrix-host01.talk.srvfarm.net",
        "matrix-host02.talk.srvfarm.net",
        "matrix-host03.talk.srvfarm.net",
    )

    all_zones = Domains.objects.filter(type="NATIVE").exclude(name__endswith=".arpa")

    for zone in all_zones:
        domain_data = dict()
        domain_data["rrsets"] = list()

        # Check for matrix SRV record
        try:
            matrix_srv = zone.records_set.get(
                name="_matrix._tcp." + zone.name, type="SRV"
            )
        except Records.MultipleObjectsReturned:
            pass
        except Records.DoesNotExist:
            record = dict()
            record["name"] = "_matrix._tcp." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "SRV"
            record["ttl"] = 3600

            record["records"] = list()

            for matrix_server in matrix_servers:
                if not matrix_server.endswith("."):
                    matrix_server = matrix_server + "."

                new_record = {"content": "10 5 443 " + matrix_server, "disabled": False}

                record["records"].append(new_record)

            domain_data["rrsets"].append(record)
        else:
            pass

        if len(domain_data["rrsets"]) > 0:
            replace2_on_powerdns_api("zones/" + zone.name, "localhost", domain_data)

    return


@shared_task(name="whstack_pdns_add_sip")
def whstack_pdns_add_sip():
    sip_servers = (
        "sip-host01.sip.srvfarm.net",
        "sip-host02.sip.srvfarm.net",
        "sip-host03.sip.srvfarm.net",
    )

    all_zones = Domains.objects.filter(type="NATIVE").exclude(name__endswith=".arpa")

    for zone in all_zones:
        domain_data = dict()
        domain_data["rrsets"] = list()

        # Check for sip SRV record
        try:
            sip_srv = zone.records_set.get(name="_sip._tcp." + zone.name, type="SRV")
        except Records.MultipleObjectsReturned:
            pass
        except Records.DoesNotExist:
            record = dict()
            record["name"] = "_sip._tcp." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "SRV"
            record["ttl"] = 3600

            record["records"] = list()

            for sip_server in sip_servers:
                if not sip_server.endswith("."):
                    sip_server = sip_server + "."

                new_record = {"content": "10 1 5060 " + sip_server, "disabled": False}

                record["records"].append(new_record)

            domain_data["rrsets"].append(record)
        else:
            pass

        # Check for sip SRV record
        try:
            sip_srv = zone.records_set.get(name="_sip._udp." + zone.name, type="SRV")
        except Records.MultipleObjectsReturned:
            pass
        except Records.DoesNotExist:
            record = dict()
            record["name"] = "_sip._udp." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "SRV"
            record["ttl"] = 3600

            record["records"] = list()

            for sip_server in sip_servers:
                if not sip_server.endswith("."):
                    sip_server = sip_server + "."

                new_record = {"content": "10 1 5060 " + sip_server, "disabled": False}

                record["records"].append(new_record)

            domain_data["rrsets"].append(record)
        else:
            pass

        # Check for sip SRV record
        try:
            sip_srv = zone.records_set.get(name="_sip._tls." + zone.name, type="SRV")
        except Records.MultipleObjectsReturned:
            pass
        except Records.DoesNotExist:
            record = dict()
            record["name"] = "_sip._tls." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "SRV"
            record["ttl"] = 3600

            record["records"] = list()

            for sip_server in sip_servers:
                if not sip_server.endswith("."):
                    sip_server = sip_server + "."

                new_record = {"content": "10 1 5061 " + sip_server, "disabled": False}

                record["records"].append(new_record)

            domain_data["rrsets"].append(record)
        else:
            pass

        # Check for sips SRV record
        try:
            sip_srv = zone.records_set.get(name="_sips._tcp." + zone.name, type="SRV")
        except Records.MultipleObjectsReturned:
            pass
        except Records.DoesNotExist:
            record = dict()
            record["name"] = "_sips._tcp." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "SRV"
            record["ttl"] = 3600

            record["records"] = list()

            for sip_server in sip_servers:
                if not sip_server.endswith("."):
                    sip_server = sip_server + "."

                new_record = {"content": "10 1 5061 " + sip_server, "disabled": False}

                record["records"].append(new_record)

            domain_data["rrsets"].append(record)
        else:
            pass

        # Check for sip NAPTR record
        try:
            sip_naptr = zone.records_set.get(name=zone.name, type="NAPTR")
        except Records.MultipleObjectsReturned:
            pass
        except Records.DoesNotExist:
            record = dict()
            record["name"] = zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "NAPTR"
            record["ttl"] = 3600

            record["records"] = list()

            new_record = {
                "content": '10 100 "S" "SIPS+D2T" "" _sip._tls.' + zone.name + ".",
                "disabled": False,
            }

            record["records"].append(new_record)

            new_record = {
                "content": '20 100 "S" "SIP+D2U" "" _sip._udp.' + zone.name + ".",
                "disabled": False,
            }

            record["records"].append(new_record)

            new_record = {
                "content": '30 100 "S" "SIP+D2T" "" _sip._tcp.' + zone.name + ".",
                "disabled": False,
            }

            record["records"].append(new_record)

            domain_data["rrsets"].append(record)
        else:
            pass

        if len(domain_data["rrsets"]) > 0:
            replace2_on_powerdns_api("zones/" + zone.name, "localhost", domain_data)

    return


@shared_task(name="whstack_pdns_add_sipfederation")
def whstack_pdns_add_sipfederation():
    all_zones = Domains.objects.filter(type="NATIVE").exclude(name__endswith=".arpa")

    for zone in all_zones:
        domain_data = dict()
        domain_data["rrsets"] = list()

        # Check for sip CNAME record
        try:
            sip_cname = zone.records_set.get(name="sip." + zone.name, type="CNAME")
        except Records.MultipleObjectsReturned:
            pass
        except Records.DoesNotExist:
            record = dict()
            record["name"] = "sip." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "CNAME"
            record["ttl"] = 3600

            record["records"] = list()

            new_record = {"content": "sipfed.sip.srvfarm.net.", "disabled": False}

            record["records"].append(new_record)

            domain_data["rrsets"].append(record)
        else:
            pass

        # Check for sipfederationtls SRV record
        try:
            sip_srv = zone.records_set.get(
                name="_sipfederationtls._tcp." + zone.name, type="SRV"
            )
        except Records.MultipleObjectsReturned:
            pass
        except Records.DoesNotExist:
            record = dict()
            record["name"] = "_sipfederationtls._tcp." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "SRV"
            record["ttl"] = 3600

            record["records"] = list()

            new_record = {
                "content": "0 100 5061 " + "sipfed." + zone.name + ".",
                "disabled": False,
            }

            record["records"].append(new_record)

            domain_data["rrsets"].append(record)
        else:
            pass

        if len(domain_data["rrsets"]) > 0:
            replace2_on_powerdns_api("zones/" + zone.name, "localhost", domain_data)

    return


@shared_task(name="whstack_pdns_add_h323")
def whstack_pdns_add_h323():
    h323_servers = (
        "h323-host01.h323.srvfarm.net",
        "h323-host02.h323.srvfarm.net",
        "h323-host03.h323.srvfarm.net",
    )

    all_zones = Domains.objects.filter(type="NATIVE").exclude(name__endswith=".arpa")

    for zone in all_zones:
        domain_data = dict()
        domain_data["rrsets"] = list()

        # Check for h323 SRV record
        try:
            h323_srv = zone.records_set.get(
                name="_h323ls._udp." + zone.name, type="SRV"
            )
        except Records.MultipleObjectsReturned:
            pass
        except Records.DoesNotExist:
            record = dict()
            record["name"] = "_h323ls._udp." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "SRV"
            record["ttl"] = 3600

            record["records"] = list()

            for h323_server in h323_servers:
                if not h323_server.endswith("."):
                    h323_server = h323_server + "."

                new_record = {"content": "10 0 1719 " + h323_server, "disabled": False}

                record["records"].append(new_record)

            domain_data["rrsets"].append(record)
        else:
            pass

        # Check for h323 SRV record
        try:
            h323_srv = zone.records_set.get(
                name="_h323rs._udp." + zone.name, type="SRV"
            )
        except Records.MultipleObjectsReturned:
            pass
        except Records.DoesNotExist:
            record = dict()
            record["name"] = "_h323rs._udp." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "SRV"
            record["ttl"] = 3600

            record["records"] = list()

            for h323_server in h323_servers:
                if not h323_server.endswith("."):
                    h323_server = h323_server + "."

                new_record = {"content": "10 0 1719 " + h323_server, "disabled": False}

                record["records"].append(new_record)

            domain_data["rrsets"].append(record)
        else:
            pass

        # Check for h323 SRV record
        try:
            h323_srv = zone.records_set.get(
                name="_h323cs._tcp." + zone.name, type="SRV"
            )
        except Records.MultipleObjectsReturned:
            pass
        except Records.DoesNotExist:
            record = dict()
            record["name"] = "_h323cs._tcp." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "SRV"
            record["ttl"] = 3600

            record["records"] = list()

            for h323_server in h323_servers:
                if not h323_server.endswith("."):
                    h323_server = h323_server + "."

                new_record = {"content": "10 0 1720 " + h323_server, "disabled": False}

                record["records"].append(new_record)

            domain_data["rrsets"].append(record)
        else:
            pass

        if len(domain_data["rrsets"]) > 0:
            replace2_on_powerdns_api("zones/" + zone.name, "localhost", domain_data)

    return


@shared_task(name="whstack_pdns_add_iax")
def whstack_pdns_add_iax():
    iax_servers = (
        "iax-host01.iax.srvfarm.net",
        "iax-host02.iax.srvfarm.net",
        "iax-host03.iax.srvfarm.net",
    )

    all_zones = Domains.objects.filter(type="NATIVE").exclude(name__endswith=".arpa")

    for zone in all_zones:
        domain_data = dict()
        domain_data["rrsets"] = list()

        # Check for iax SRV record
        try:
            iax_srv = zone.records_set.get(name="_iax._udp." + zone.name, type="SRV")
        except Records.MultipleObjectsReturned:
            pass
        except Records.DoesNotExist:
            record = dict()
            record["name"] = "_iax._udp." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "SRV"
            record["ttl"] = 3600

            record["records"] = list()

            for iax_server in iax_servers:
                if not iax_server.endswith("."):
                    iax_server = iax_server + "."

                new_record = {"content": "10 1 4569 " + iax_server, "disabled": False}

                record["records"].append(new_record)

            domain_data["rrsets"].append(record)
        else:
            pass

        if len(domain_data["rrsets"]) > 0:
            replace2_on_powerdns_api("zones/" + zone.name, "localhost", domain_data)

    return


@shared_task(name="whstack_pdns_add_mumble")
def whstack_pdns_add_mumble():
    mumble_servers = ("mumble-host01.mumble.srvfarm.net",)

    all_zones = Domains.objects.filter(type="NATIVE").exclude(name__endswith=".arpa")

    for zone in all_zones:
        domain_data = dict()
        domain_data["rrsets"] = list()

        # Check for mumble SRV record
        try:
            mumble_srv = zone.records_set.get(
                name="_mumble._tcp." + zone.name, type="SRV"
            )
        except Records.MultipleObjectsReturned:
            pass
        except Records.DoesNotExist:
            record = dict()
            record["name"] = "_mumble._tcp." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "SRV"
            record["ttl"] = 3600

            record["records"] = list()

            for mumble_server in mumble_servers:
                if not mumble_server.endswith("."):
                    mumble_server = mumble_server + "."

                new_record = {
                    "content": "0 5 64738 " + mumble_server,
                    "disabled": False,
                }

                record["records"].append(new_record)

            domain_data["rrsets"].append(record)
        else:
            pass

        if len(domain_data["rrsets"]) > 0:
            replace2_on_powerdns_api("zones/" + zone.name, "localhost", domain_data)

    return


@shared_task(name="whstack_pdns_add_mqtt")
def whstack_pdns_add_mqtt():
    mqtt_servers = (
        "mqtt-host01.mqtt.srvfarm.net",
        "mqtt-host02.mqtt.srvfarm.net",
        "mqtt-host03.mqtt.srvfarm.net",
    )

    all_zones = Domains.objects.filter(type="NATIVE").exclude(name__endswith=".arpa")

    for zone in all_zones:
        domain_data = dict()
        domain_data["rrsets"] = list()

        # Check for mqtt SRV record
        try:
            mqtt_srv = zone.records_set.get(name="_mqtt._tcp." + zone.name, type="SRV")
        except Records.MultipleObjectsReturned:
            pass
        except Records.DoesNotExist:
            record = dict()
            record["name"] = "_mqtt._tcp." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "SRV"
            record["ttl"] = 3600

            record["records"] = list()

            for mqtt_server in mqtt_servers:
                if not mqtt_server.endswith("."):
                    mqtt_server = mqtt_server + "."

                new_record = {"content": "0 5 1883 " + mqtt_server, "disabled": False}

                record["records"].append(new_record)

            domain_data["rrsets"].append(record)
        else:
            pass

        # Check for secure-mqtt SRV record
        try:
            mqtt_srv = zone.records_set.get(
                name="_secure-mqtt._tcp." + zone.name, type="SRV"
            )
        except Records.MultipleObjectsReturned:
            pass
        except Records.DoesNotExist:
            record = dict()
            record["name"] = "_secure-mqtt._tcp." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "SRV"
            record["ttl"] = 3600

            record["records"] = list()

            for mqtt_server in mqtt_servers:
                if not mqtt_server.endswith("."):
                    mqtt_server = mqtt_server + "."

                new_record = {"content": "0 5 8883 " + mqtt_server, "disabled": False}

                record["records"].append(new_record)

            domain_data["rrsets"].append(record)
        else:
            pass

        if len(domain_data["rrsets"]) > 0:
            replace2_on_powerdns_api("zones/" + zone.name, "localhost", domain_data)

    return


@shared_task(name="whstack_pdns_sync_subzone_ns")
def whstack_pdns_sync_subzone_ns():
    all_zones = Domains.objects.filter(type="NATIVE").all()

    for zone in all_zones:
        domain_data = dict()
        domain_data["rrsets"] = list()

        if zone.name == ".":
            continue

        zone_parts = zone.name.split(".")

        parent_zone_parts = zone_parts
        del parent_zone_parts[0]

        if len(parent_zone_parts) < 2:
            # skip root tld zones
            continue

        parent_zone = ".".join(parent_zone_parts)

        print(zone.name + " is part of " + parent_zone + ".")

        # is parent_zone available locally?
        try:
            parent_zone_domain = Domains.objects.get(name=parent_zone)
        except Domains.DoesNotExist:
            # root is not local, skip
            continue

        print(parent_zone + " is local.")

        record = dict()
        record["name"] = zone.name + "."
        record["changetype"] = "REPLACE"
        record["type"] = "NS"
        record["ttl"] = 3600

        record["records"] = list()

        missing_ns_counter = 0

        for nameserver in zone.records_set.filter(name=zone.name, type="NS"):
            nameserver = nameserver.content

            try:
                ns_exists = parent_zone_domain.records_set.get(
                    name=zone.name, type="NS", content=nameserver
                )
            except Records.DoesNotExist:
                missing_ns_counter += 1
            else:
                # NS already set
                continue

        if missing_ns_counter == 0:
            # no NS missing
            continue

        for nameserver in zone.records_set.filter(name=zone.name, type="NS"):
            nameserver = nameserver.content

            if not nameserver.endswith("."):
                nameserver = nameserver + "."

            new_record = {"content": nameserver, "disabled": False}

            print("Adding " + zone.name + " NS " + nameserver + " to " + parent_zone)

            record["records"].append(new_record)

        domain_data["rrsets"].append(record)

        if len(domain_data["rrsets"]) > 0:
            replace2_on_powerdns_api("zones/" + parent_zone, "localhost", domain_data)

    return


@shared_task(name="whstack_pdns_sync_subzone_ds")
def whstack_pdns_sync_subzone_ds():
    all_zones = Domains.objects.filter(type="NATIVE").all()

    for zone in all_zones:
        domain_data = dict()
        domain_data["rrsets"] = list()

        if zone.name == ".":
            continue

        zone_parts = zone.name.split(".")

        parent_zone_parts = zone_parts
        del parent_zone_parts[0]

        if len(parent_zone_parts) < 2:
            # skip root tld zones
            continue

        parent_zone = ".".join(parent_zone_parts)

        print(zone.name + " is part of " + parent_zone + ".")

        # is parent_zone available locally?
        try:
            parent_zone_domain = Domains.objects.get(name=parent_zone)
        except Domains.DoesNotExist:
            # root is not local, skip
            continue

        print(parent_zone + " is local.")

        record = dict()
        record["name"] = zone.name + "."
        record["changetype"] = "REPLACE"
        record["type"] = "DS"
        record["ttl"] = 3600

        record["records"] = list()

        missing_ds_counter = 0
        ksk_list = list()

        # fetch ds-records
        crypto_key_list = get_from_powerdns_api(
            "zones", "localhost", zone.name + "/cryptokeys"
        )

        if "error" in crypto_key_list:
            continue

        for crypto_key in crypto_key_list:
            if crypto_key["keytype"] == "ksk":
                ksk_list.extend(crypto_key["ds"])

        if len(ksk_list) < 1:
            continue

        for ksk in ksk_list:
            try:
                ds_exists = parent_zone_domain.records_set.get(
                    name=zone.name, type="DS", content=ksk
                )
            except Records.DoesNotExist:
                missing_ds_counter += 1
            else:
                # DS already set
                continue

        if missing_ds_counter == 0:
            # no DS missing
            continue

        for ksk in ksk_list:
            new_record = {"content": ksk, "disabled": False}

            print("Adding " + zone.name + " DS '" + ksk + "' to " + parent_zone)

            record["records"].append(new_record)

        domain_data["rrsets"].append(record)

        if len(domain_data["rrsets"]) > 0:
            replace2_on_powerdns_api("zones/" + parent_zone, "localhost", domain_data)

    return


@shared_task(name="whstack_pdns_sync_nameservers")
def whstack_pdns_sync_nameservers():
    registry_domains = WhRegistryDomains.objects.all()

    for domain in registry_domains:
        domain_nameservers = list()

        try:
            zone = Domains.objects.get(name=domain.domain)
        except Domains.DoesNotExist:
            continue

        for nameserver in zone.records_set.filter(name=domain.domain, type="NS"):
            domain_nameservers.append(nameserver.content)

        if len(domain_nameservers) > 0:
            payload = {
                "s_login": settings.RRPPROXY_USER,
                "s_pw": settings.RRPPROXY_PASSWD,
                "command": "ModifyDomain",
                "domain": domain.domain,
            }

            nameserver_count = 0

            for nameserver in domain_nameservers:
                payload["nameserver" + str(nameserver_count)] = nameserver
                nameserver_count += 1

            rrp_request = requests.get(
                "https://api.rrpproxy.net/api/call", params=payload
            )
        else:
            continue

    return


@shared_task(name="whstack_pdns_sync_default_template")
def whstack_pdns_sync_default_template():
    default_zone_name = "default.zone-templates.local"
    active_tenants = WhTenantSettings.objects.filter(dns_enabled=True)

    for active_tenant in active_tenants:
        default_customer_zone_str = (
            str(active_tenant.customer.id) + "-default.zone-templates.local"
        )

        try:
            default_customer_zone = Domains.objects.get(name=default_customer_zone_str)
        except Domains.DoesNotExist:
            # Create new zone
            new_zone = create_zone_from_template(
                default_zone_name, default_customer_zone_str
            )

    return


@shared_task(name="whstack_pdns_sync_anycast_ns")
def whstack_pdns_sync_anycast_ns():
    all_zones = Domains.objects.filter(type="NATIVE").all()

    ns_list_anycast_false = [
        "ns01.srvfarm.net",
        "ns02.srvfarm.net",
    ]

    ns_list_anycast_true = [
        "ns01.srvfarm.net",
        "ns02.srvfarm.net",
        "ns1.dnsres.net",
        "ns2.dnsres.net",
        "ns3.dnsres.net",
    ]

    for zone in all_zones:
        try:
            anycast_metadata = zone.domainmetadata_set.get(kind="X-ANYCAST-ENABLED")
        except Domainmetadata.DoesNotExist:
            anycast_metadata = Domainmetadata(
                domain=zone, kind="X-ANYCAST-ENABLED", content="0"
            )
            anycast_metadata.save(force_insert=True)

        # TODO: only update if changed

        domain_data = dict()
        domain_data["rrsets"] = list()

        record = dict()
        record["name"] = zone.name + "."
        record["changetype"] = "REPLACE"
        record["type"] = "NS"
        record["ttl"] = 3600
        record["records"] = list()

        if anycast_metadata.content == "1":
            # replace NS by ns_list_anycast_true if diff
            for nameserver in ns_list_anycast_true:
                new_record = {"content": f"{nameserver}.", "disabled": False}
                record["records"].append(new_record)
        else:
            # replace NS by ns_list_anycast_false if diff
            for nameserver in ns_list_anycast_false:
                new_record = {"content": f"{nameserver}.", "disabled": False}
                record["records"].append(new_record)

        domain_data["rrsets"].append(record)

        if len(domain_data["rrsets"]) > 0:
            replace2_on_powerdns_api("zones/" + zone.name, "localhost", domain_data)

    return


@shared_task(name="whstack_pdns_add_autodiscover")
def whstack_pdns_add_autodiscover():
    all_zones = Domains.objects.filter(type="NATIVE").exclude(name__endswith=".arpa")

    for zone in all_zones:
        domain_data = dict()
        domain_data["rrsets"] = list()

        # Check for autodiscover CNAME record
        try:
            autodiscover_cname = zone.records_set.get(
                name="autodiscover." + zone.name, type="CNAME"
            )
        except Records.MultipleObjectsReturned:
            pass
        except Records.DoesNotExist:
            record = dict()
            record["name"] = "autodiscover." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "CNAME"
            record["ttl"] = 3600

            record["records"] = list()

            new_record = {
                "content": "autodiscover-redir.srvfarm.net.",
                "disabled": False,
            }

            record["records"].append(new_record)

            domain_data["rrsets"].append(record)
        else:
            pass

        # Check for autoconfig CNAME record
        try:
            autoconfig_cname = zone.records_set.get(
                name="autoconfig." + zone.name, type="CNAME"
            )
        except Records.MultipleObjectsReturned:
            pass
        except Records.DoesNotExist:
            record = dict()
            record["name"] = "autoconfig." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "CNAME"
            record["ttl"] = 3600

            record["records"] = list()

            new_record = {"content": "autoconfig-redir.srvfarm.net.", "disabled": False}

            record["records"].append(new_record)

            domain_data["rrsets"].append(record)
        else:
            pass

        if len(domain_data["rrsets"]) > 0:
            replace2_on_powerdns_api("zones/" + zone.name, "localhost", domain_data)

    return


@shared_task(name="whstack_pdns_add_sso")
def whstack_pdns_add_sso():
    default_sso_url = "https://sso.srvfarm.net/saml/singleSignOn"

    all_zones = Domains.objects.filter(type="NATIVE").exclude(name__endswith=".arpa")

    for zone in all_zones:
        domain_data = dict()
        domain_data["rrsets"] = list()

        if zone.name == ".":
            continue

        domain_sso_url = default_sso_url + "?domain=" + zone.name
        domain_sso_record = '"DirectFedAuthUrl=' + domain_sso_url + '"'

        try:
            direct_fed_auth_url = zone.records_set.get(
                type="TXT",
                name=zone.name,
                content__istartswith='"DirectFedAuthUrl=',
            )
        except Records.MultipleObjectsReturned:
            continue
        except Records.DoesNotExist:
            record = dict()
            record["name"] = zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "TXT"
            record["ttl"] = 3600

            record["records"] = list()

            for existing_txt_record in zone.records_set.filter(
                type="TXT", name=zone.name
            ):
                record["records"].append(
                    {"content": existing_txt_record.content, "disabled": False}
                )

            record["records"].append({"content": domain_sso_record, "disabled": False})

            domain_data["rrsets"].append(record)

        if len(domain_data["rrsets"]) > 0:
            replace2_on_powerdns_api("zones/" + zone.name, "localhost", domain_data)

    return


@shared_task(name="whstack_pdns_add_microsoft_enrollment")
def whstack_pdns_add_microsoft_enrollment():
    cname_enterprise_enrollment_default = "enterpriseenrollment-s.manage.microsoft.com"
    cname_enterprise_registration_default = "enterpriseregistration.windows.net"

    all_zones = Domains.objects.filter(type="NATIVE").exclude(name__endswith=".arpa")

    for zone in all_zones:
        domain_data = dict()
        domain_data["rrsets"] = list()

        if zone.name == ".":
            continue

        try:
            cname_enterprise_enrollment = zone.records_set.get(
                type="CNAME",
                name="enterpriseenrollment." + zone.name,
            )
        except Records.MultipleObjectsReturned:
            continue
        except Records.DoesNotExist:
            record = dict()
            record["name"] = "enterpriseenrollment." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "CNAME"
            record["ttl"] = 3600

            record["records"] = list()

            record["records"].append(
                {
                    "content": cname_enterprise_enrollment_default + ".",
                    "disabled": False,
                }
            )

            domain_data["rrsets"].append(record)

        try:
            cname_enterprise_registration = zone.records_set.get(
                type="CNAME",
                name="enterpriseregistration." + zone.name,
            )
        except Records.MultipleObjectsReturned:
            continue
        except Records.DoesNotExist:
            record = dict()
            record["name"] = "enterpriseregistration." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "CNAME"
            record["ttl"] = 3600

            record["records"] = list()

            record["records"].append(
                {
                    "content": cname_enterprise_registration_default + ".",
                    "disabled": False,
                }
            )

            domain_data["rrsets"].append(record)

        if len(domain_data["rrsets"]) > 0:
            replace2_on_powerdns_api("zones/" + zone.name, "localhost", domain_data)

    return


@shared_task(name="whstack_pdns_add_acme_challenge")
def whstack_pdns_add_acme_challenge():
    default_acme_challenge_server = "acme.instant-tls.com"

    all_zones = Domains.objects.filter(type="NATIVE").exclude(name__endswith=".arpa")

    for zone in all_zones:
        domain_data = dict()
        domain_data["rrsets"] = list()

        if zone.name == ".":
            continue

        zone_acme_challenge_server = zone.name + "." + default_acme_challenge_server

        try:
            root_acme_challenge = zone.records_set.get(
                name="_acme-challenge." + zone.name
            )
        except Records.MultipleObjectsReturned:
            continue
        except Records.DoesNotExist:
            record = dict()
            record["name"] = "_acme-challenge." + zone.name + "."
            record["changetype"] = "REPLACE"
            record["type"] = "CNAME"
            record["ttl"] = 3600

            record["records"] = list()

            record["records"].append(
                {"content": zone_acme_challenge_server, "disabled": False}
            )

            domain_data["rrsets"].append(record)

        if len(domain_data["rrsets"]) > 0:
            replace2_on_powerdns_api("zones/" + zone.name, "localhost", domain_data)

    return


@shared_task(name="whstack_pdns_add_dkim")
def whstack_pdns_add_dkim():
    dkim_records = {
        "srvfarm1": "srvfarm1.dkim.srvfarm.net",
        "srvfarm2": "srvfarm2.dkim.srvfarm.net",
    }

    all_zones = Domains.objects.filter(type="NATIVE").exclude(name__endswith=".arpa")

    for zone in all_zones:
        domain_data = dict()
        domain_data["rrsets"] = list()

        for dkim_key, dkim_dst_cname in dkim_records.items():
            record_name = f"{dkim_key}._domainkey.{zone.name}"
            # Check for dkim CNAME record
            try:
                dkim_cname = zone.records_set.get(name=record_name, type="CNAME")
            except Records.MultipleObjectsReturned:
                pass
            except Records.DoesNotExist:
                record = dict()
                record["name"] = record_name + "."
                record["changetype"] = "REPLACE"
                record["type"] = "CNAME"
                record["ttl"] = 3600

                record["records"] = list()

                new_record = {
                    "content": f"{dkim_dst_cname}.",
                    "disabled": False,
                }

                record["records"].append(new_record)

                domain_data["rrsets"].append(record)
            else:
                pass

        if len(domain_data["rrsets"]) > 0:
            replace2_on_powerdns_api("zones/" + zone.name, "localhost", domain_data)

    return
